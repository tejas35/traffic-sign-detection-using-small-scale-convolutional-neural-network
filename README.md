# Traffic sign recognition using small-scale colvolutional neural network

## Dataset

`German-traffic sign recognition benchmark`

> 43 classes

> more than 50000 images in total


https://gitlab.com/tejas35/gtsrb.git